
import org.openkinect.processing.*; //Lib para Kinect
//import org.opencamera.processing.*; //Lib para Câmera comum

Kinect camera;
PImage previous;
PImage next;
color changed;
int cycles;
int delay;

void setup()
{
  size(640, 480);      //Inicia tela com resolução da camera do Kinect 
  previous = createImage(width, height, RGB); //Inicializa uma imagem temporária
  camera = new Kinect(this);
  camera.initDepth(); //Inicia sensor de distância infravermelho
  changed = color(250, 100, 200); //Rosa
  cycles = 0;
  delay = 50;
  camera.setTilt(25); //25 graus - posição padrão
}

void draw()
{
  background(0); // Fundo preto
  next = camera.getDepthImage(); //Captura imagem 3D

  for (int x=0; x < next.width; x++) {
    for (int y=0; y < next.height; y++) {
      int index = x + (y*next.width);
      if (next.pixels[index] != previous.pixels[index]) {
        next.pixels[index] = changed;   //Se Os pixels são diferentes, fica rosa
        //println(next.pixels[x]);
      }
    }
  }

  image(next, 0, 0); // Mostra imagem no ponto (0, 0)

  if (cycles % delay == 0) {
    previous = next.get(); //Atualiza a imagem temporária em intervalos de ciclos
  }

  cycles++;
}
